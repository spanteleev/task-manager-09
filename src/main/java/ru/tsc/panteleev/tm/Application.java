package ru.tsc.panteleev.tm;

import ru.tsc.panteleev.tm.component.Bootstrap;
import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
