package ru.tsc.panteleev.tm.api;

import ru.tsc.panteleev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
