package ru.tsc.panteleev.tm.service;

import ru.tsc.panteleev.tm.api.ICommandRepository;
import ru.tsc.panteleev.tm.api.ICommandService;
import ru.tsc.panteleev.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
