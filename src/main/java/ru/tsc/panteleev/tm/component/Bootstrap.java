package ru.tsc.panteleev.tm.component;

import ru.tsc.panteleev.tm.api.ICommandController;
import ru.tsc.panteleev.tm.api.ICommandRepository;
import ru.tsc.panteleev.tm.api.ICommandService;
import ru.tsc.panteleev.tm.constant.ArgumentConst;
import ru.tsc.panteleev.tm.constant.TerminalConst;
import ru.tsc.panteleev.tm.controller.CommandController;
import ru.tsc.panteleev.tm.repository.CommandRepository;
import ru.tsc.panteleev.tm.service.CommandService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(String[] args) throws IOException {
        if (runWithArgument(args))
            System.exit(0);
        commandController.showWelcome();
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Enter command:");
            String command = bufferedReader.readLine();
            runWithCommand(command);
        }
    }

    public void runWithCommand(String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public boolean runWithArgument(String[] args) {
        if (args == null || args.length == 0) return false;
        final String command = args[0];
        if (command == null || command.isEmpty()) return false;
        runWithArgument(command);
        return true;
    }

    public void runWithArgument(String command) {
        switch (command) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument(command);
                break;
        }
    }

    public static void exit() {
        System.exit(0);
    }

}
